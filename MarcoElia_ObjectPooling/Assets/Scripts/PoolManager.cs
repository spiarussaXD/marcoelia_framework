﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PoolItem
{
    public GameObject prefab;
    public int poolAmount;
    public bool expand;
}

public class PoolManager : MonoBehaviour
{

    public static PoolManager sharedInstance;
    public List<PoolItem> itemsToPool;
    public List<GameObject> pool;

    void Awake()
    {
        sharedInstance = this;
    }

    // Use this for initialization
    void Start()
    {
        pool = new List<GameObject>();
        foreach (PoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.poolAmount; i++)
            {
                GameObject obj = Instantiate(item.prefab);
                obj.SetActive(false);
                pool.Add(obj);
            }
        }
    }

    public GameObject GetPooledObject(string tag)
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (!pool[i].activeInHierarchy && pool[i].tag == tag)
            {
                return pool[i];
            }
        }
        foreach (PoolItem item in itemsToPool)
        {
            if (item.prefab.tag == tag)
            {
                if (item.expand)
                {
                    GameObject obj = Instantiate(item.prefab);
                    obj.SetActive(false);
                    pool.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }
}
